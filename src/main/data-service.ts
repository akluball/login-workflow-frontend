import axios, { AxiosResponse, AxiosInstance } from 'axios';

function allowAllValidator(status: number): boolean {
    return true;
}

const axiosInstance = axios.create({
    baseURL: `${process.env.BACKEND_URI}/api`,
    validateStatus: allowAllValidator,
    headers: {
        'Content-Type': 'application/json'
    }
});

export interface Registration {
    firstName: string;
    lastName: string;
    handle: string;
    password: string;
    email: string;
    [key: string]: string;
}

export async function register(pendingUser: Registration) {
    return await axiosInstance.post('/registration', pendingUser);
}

export async function verify(email: string, verificationCode: string): Promise<AxiosResponse> {
    return await axiosInstance.post(`/registration/${email}/verification`, verificationCode);
}

export async function login(identifier: string, password: string): Promise<AxiosResponse> {
    return await axiosInstance.post(`/login/${identifier}`, password, { withCredentials: true });
}

export async function logout(): Promise<void> {
    return await axiosInstance.post('/logout', null, { withCredentials: true });
}

export async function token(): Promise<AxiosResponse> {
    return await axiosInstance.get('/token', { withCredentials: true });
}

export async function currentUser(token: string): Promise<AxiosResponse> {
    const headers = { 'Authorization': `Bearer ${token}` };
    return await axiosInstance.get('/user', { headers });
}