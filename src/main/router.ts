import Vue from 'vue';
import VueRouter, { RouteConfig, RouteRecord } from 'vue-router';
import store from './store';

import LogIn from './components/LogIn.vue';
import PageNotFound from './components/PageNotFound.vue';
import RegisterInput from './components/RegisterInput.vue';
import RegisterNameInput from './components/RegisterNameInput.vue';
import RegisterCredInput from './components/RegisterCredInput.vue';
import RegisterEmailInput from './components/RegisterEmailInput.vue';
import RegisterVerify from './components/RegisterVerify.vue';
import RegisterSuccess from './components/RegisterSuccess.vue';
import Home from './components/Home.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
    {
        path: '',
        redirect: 'home'
    },
    {
        path: '/login',
        component: LogIn,
        meta: { titlePostfix: 'Log In' },
        async beforeEnter(to, from, next) {
            if (store.getters.hasToken) {
                next('/home');
            } else {
                await store.dispatch('getToken');
                if (store.getters.hasToken) {
                    await store.dispatch('getCurrentUser');
                    next('/home');
                } else {
                    next();
                }
            }
        }
    },
    { path: '/register', redirect: '/register/input/name' },
    {
        path: '/register/input',
        component: RegisterInput,
        meta: { titlePostfix: 'Register Input' },
        children: [
            {
                path: 'name',
                component: RegisterNameInput
            },
            {
                path: 'credentials',
                component: RegisterCredInput
            },
            {
                path: 'email',
                component: RegisterEmailInput
            },
        ]
    },
    {
        path: '/register/verify/:email',
        component: RegisterVerify,
        meta: { titlePostfix: 'Register Verify' },
        props: true
    },
    {
        path: '/register/success',
        component: RegisterSuccess,
        meta: { titlePostfix: 'Register Success' },
    },
    {
        path: '/home',
        component: Home,
        meta: { titlePostfix: 'Home' }
    },
    {
        path: '*',
        component: PageNotFound,
        meta: { titlePostfix: 'Page not Found' }
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

const noAuthWhitelist = [
    /^\/login(?:\/.*)?/,
    /^\/register(?:\/.*)?/
];

// check if page requires user to be logged in
router.beforeEach(async (to, from, next) => {
    const doesNotRequireLogin = noAuthWhitelist.findIndex(routeRegex => routeRegex.test(to.fullPath)) !== -1;
    if (doesNotRequireLogin) {
        next();
    } else {
        if (store.getters.hasToken) {
            next();
        } else {
            next('/login');
        }
    }
});

// add title postfix if present
router.beforeEach((to, from, next) => {
    const routeWithTitlePostfix = to.matched.slice()
        .reverse()
        .find(route => !!route.meta.titlePostfix);
    const titlePostfix = (routeWithTitlePostfix === undefined) ? '' : ` - ${routeWithTitlePostfix.meta.titlePostfix}`
    document.title = `Login Workflow${titlePostfix}`;
    next();
});

export default router;