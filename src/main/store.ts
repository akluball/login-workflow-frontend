import Vue from 'vue';
import Vuex, { Store, StoreOptions } from 'vuex';
import * as ds from './data-service';
import { LoginWorkflowState, LoginWorkflowUser } from './types';
import { AxiosResponse } from 'axios';
import { is2xx } from './utils';

Vue.use(Vuex);

let refreshTokenTimeoutId: number | undefined;
function clearRefreshTokenTimeout() {
    if (refreshTokenTimeoutId !== undefined) {
        window.clearTimeout(refreshTokenTimeoutId);
    }
}

const storeOptions: StoreOptions<LoginWorkflowState> = {
    state: {
        token: undefined,
        currentUser: undefined
    },
    getters: {
        hasToken(state: LoginWorkflowState) {
            return state.token !== undefined && state.token.length > 0
        }
    },
    mutations: {
        token(state: LoginWorkflowState, token) {
            clearRefreshTokenTimeout();
            state.token = token
        },
        forgetToken(state: LoginWorkflowState) {
            clearRefreshTokenTimeout();
            state.token = undefined;
        },
        currentUser(state: LoginWorkflowState, currentUser: LoginWorkflowUser) {
            state.currentUser = currentUser;
        }
    },
    actions: {
        async login({ dispatch, getters }, { identifier, password }) {
            const loginResponse = await ds.login(identifier, password);
            if (is2xx(loginResponse)) {
                await dispatch('getToken');
                if (getters.hasToken) {
                    await dispatch('getCurrentUser');
                }
            }
        },
        async getToken({ commit, dispatch }) {
            const tokenResponse = await ds.token();
            if (is2xx(tokenResponse)) {
                const token = tokenResponse.data;
                commit('token', token);
                const expirationEpochSeconds = JSON.parse(atob(token.split('.')[1])).exp;
                if (Number.isInteger(expirationEpochSeconds)) {
                    const millisToExpiration = (1000 * expirationEpochSeconds) - new Date().getTime();
                    refreshTokenTimeoutId = window.setTimeout(() => dispatch("getToken"), millisToExpiration - 5);
                }
            }
        },
        async getCurrentUser({ commit, getters, state }) {
            if (getters.hasToken) {
                const currentUserResponse = await ds.currentUser(state.token as string);
                if (is2xx(currentUserResponse)) {
                    commit('currentUser', currentUserResponse.data)
                }
            }
        }
    }
};

const store: Store<LoginWorkflowState> = new Vuex.Store(storeOptions);

export default store;