export interface LoginWorkflowState {
    token: string | undefined;
    currentUser: LoginWorkflowUser | undefined;
};

export interface LoginWorkflowUser {
    id: number;
    handle: string;
    firstName: string;
    lastName: string;
    email: string;
    joinEpochSeconds: number;
};