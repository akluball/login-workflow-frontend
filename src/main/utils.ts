import { AxiosResponse } from 'axios';

export function is2xx(response: AxiosResponse) {
    return response.status.toString().startsWith('2');
}