import Vue from 'vue';
import router from './router';
import store from './store';
import './app.scss';

Vue.filter('unicode2html', (codePoint: string): string => {
    return String.fromCharCode(parseInt(codePoint));
});

new Vue({
    template: '<router-view></router-view>',
    router,
    store
}).$mount('#app');