import { driver, waiter, DriverAndWaiterContext } from './test-util';
import _test, { TestInterface } from 'ava';
import { waitingOn } from './WebDriverWaiter';
import { expect } from 'chai';
import * as backend from './backend/backend';
import * as uniqueData from './unique-data';
import { getDriver } from './utils';
import { WebElement, By, Key } from 'selenium-webdriver';
import { MSRequestMatcherBuilder, MSRequestMatcher, MSResponseBuilder, MSResponse } from './backend/mock-server';

const test = _test as TestInterface<DriverAndWaiterContext>;

test.beforeEach(t => {
    t.context.driver = getDriver();
    t.context.waiter = waitingOn(driver(t));
});

test.afterEach.always(async t => {
    await driver(t).quit();
});

test('no token, route to /login', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    await waiter(t).uriMatches(/\/login/);
    await waiter(t).title('Login Workflow - Log In');
    await backend.clear(backend.NO_COOKIES_TOKEN_MATCHER);
});

test('name input, click next', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/name`);
    const nextButton = await waiter(t).el(By.className('register-name__next'));
    await waiter(t).textMatches(nextButton, /next/);
    await nextButton.click();
    await waiter(t).uriMatches(/\/register\/input\/credentials$/);
});

test('name input, first name input, press enter', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/name`);
    await waiter(t).el(By.className('register-name__first-input'))
        .sendKeys(Key.ENTER);
    await waiter(t).uriMatches(/\/register\/input\/credentials$/);
});

test('name input, last name input, press enter', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/name`);
    await waiter(t).el(By.className('register-name__last-input'))
        .sendKeys(Key.ENTER);
    await waiter(t).uriMatches(/\/register\/input\/credentials$/);
});

test('name input, first name input label', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/name`);
    const firstNameLabel = await waiter(t).el(By.className('register-name__first'));
    await waiter(t).textMatches(firstNameLabel, /^first name/);
});

test('name input, last name input label', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/name`);
    const lastNameLabel = await waiter(t).el(By.className('register-name__last'));
    await waiter(t).textMatches(lastNameLabel, /^last name/);
});

test('creds input, click previous', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    const previousButton = await waiter(t).el(By.className('register-creds__previous'));
    await waiter(t).textMatches(previousButton, /previous/);
    await previousButton.click();
    await waiter(t).uriMatches(/\/register\/input\/name$/);
});

test('creds input, click next', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    const nextButton = await waiter(t).el(By.className('register-creds__next'));
    await waiter(t).textMatches(nextButton, /next/);
    await nextButton.click();
    await waiter(t).uriMatches(/\/register\/input\/email$/);
});

test('creds input, handle input, press enter', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    await waiter(t).el(By.className('register-creds__handle-input'))
        .sendKeys(Key.ENTER);
    await waiter(t).uriMatches(/\/register\/input\/email$/);
});

test('creds input, password input, press enter', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    await waiter(t).el(By.className('register-creds__password-input'))
        .sendKeys(Key.ENTER);
    await waiter(t).uriMatches(/\/register\/input\/email$/);
});

test('creds input, retype password input, press enter', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    await waiter(t).el(By.className('register-creds__retype-password-input'))
        .sendKeys(Key.ENTER);
    await waiter(t).uriMatches(/\/register\/input\/email$/);
});

test('creds input, handle input label', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    const handleLabel = await waiter(t).el(By.className('register-creds__handle'));
    await waiter(t).textMatches(handleLabel, /^handle/);
});

test('creds input, password input label', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    const handleLabel = await waiter(t).el(By.className('register-creds__password'));
    await waiter(t).textMatches(handleLabel, /^password/);
});

test('creds input, retype password input label', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    const handleLabel = await waiter(t).el(By.className('register-creds__retype-password'));
    await waiter(t).textMatches(handleLabel, /^retype password/);
});

test('creds input, retype does not match password', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/credentials`);
    await waiter(t).el(By.className('register-creds__password-input'))
        .sendKeys('password');
    await waiter(t).el(By.className('register-creds__retype-password-input'))
        .sendKeys('non password', Key.ENTER);
    const errors = await waiter(t).els(By.className('register-creds__error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('password does not match retyped password');
});

test('email input, click previous', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    const previousButton = await waiter(t).el(By.className('register-email__previous'));
    await waiter(t).textMatches(previousButton, /previous/);
    await previousButton.click();
    await waiter(t).uriMatches(/\/register\/input\/credentials$/);
});

test('email input, click send verification', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    expect(await driver(t).findElements(By.className('register-email__input-error'))).to.be.empty;
    const sendButton = await waiter(t).el(By.className('register-email__send'));
    await waiter(t).textMatches(sendButton, /send verification/);
    await sendButton.click();
    expect(await waiter(t).els(By.className('register-email__input-error'))).to.not.be.empty;
});

test('email input, email input, click enter', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    expect(await driver(t).findElements(By.className('register-email__input-error'))).to.be.empty;
    await waiter(t).el(By.className('register-email__email-input'))
        .sendKeys(Key.ENTER);
    expect(await waiter(t).els(By.className('register-email__input-error'))).to.not.be.empty;
});

test('email input, email input label', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    const emailLabel = await waiter(t).el(By.className('register-email__email'));
    await waiter(t).textMatches(emailLabel, /email/);
});

test('email is required', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    await waiter(t).el(By.className('register-email__send')).click();
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('email is required');
});

test('first name is required', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    await waiter(t).el(By.className('register-email__send')).click();
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('first name is required');
});

test('last name is required', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    await waiter(t).el(By.className('register-email__send')).click();
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('last name is required');
});

test('handle is required', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    await waiter(t).el(By.className('register-email__send')).click();
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('handle is required');
});

test('password is required', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    await waiter(t).el(By.className('register-email__send')).click();
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('password is required');
});

test('email input, invalid email', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/email`);
    await waiter(t).el(By.className('register-email__email-input'))
        .sendKeys('not-a-real-email', Key.ENTER);
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('invalid email');
});

test('email conflict', async t => {
    const registration = uniqueData.registration();
    const registerMatcher = new MSRequestMatcherBuilder()
        .post().path('/api/registration').json(registration).build();
    const responseMatcher = new MSResponseBuilder().status(409)
        .body(`conflict: User with email ${registration.email}`).build();
    backend.expect(registerMatcher, responseMatcher);
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/name`);
    await waiter(t).el(By.className('register-name__first-input')).sendKeys(registration.firstName);
    await waiter(t).el(By.className('register-name__last-input')).sendKeys(registration.lastName, Key.ENTER);
    await waiter(t).el(By.className('register-creds__handle-input')).sendKeys(registration.handle);
    await waiter(t).el(By.className('register-creds__password-input')).sendKeys(registration.password);
    await waiter(t).el(By.className('register-creds__retype-password-input')).sendKeys(registration.password, Key.ENTER);
    await waiter(t).el(By.className('register-email__email-input')).sendKeys(registration.email, Key.ENTER);
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('email associated with existing user or registration');
    await backend.clear(registerMatcher);
});

test('handle conflict', async t => {
    const registration = uniqueData.registration();
    const registerMatcher = new MSRequestMatcherBuilder()
        .post().path('/api/registration').json(registration).build();
    const responseMatcher = new MSResponseBuilder().status(409)
        .body(`conflict: User with handle ${registration.handle}`).build();
    backend.expect(registerMatcher, responseMatcher);
    await driver(t).get(`${process.env.FRONTEND_URI}/register/input/name`);
    await waiter(t).el(By.className('register-name__first-input')).sendKeys(registration.firstName);
    await waiter(t).el(By.className('register-name__last-input')).sendKeys(registration.lastName, Key.ENTER);
    await waiter(t).el(By.className('register-creds__handle-input')).sendKeys(registration.handle);
    await waiter(t).el(By.className('register-creds__password-input')).sendKeys(registration.password);
    await waiter(t).el(By.className('register-creds__retype-password-input')).sendKeys(registration.password, Key.ENTER);
    await waiter(t).el(By.className('register-email__email-input')).sendKeys(registration.email, Key.ENTER);
    await backend.verifyAndClear(registerMatcher);
    const errors = await waiter(t).els(By.className('register-email__input-error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('handle associated with existing user or registration');
    await backend.clear(registerMatcher);
});

test('verification description', async t => {
    const email = uniqueData.email();
    await driver(t).get(`${process.env.FRONTEND_URI}/register/verify/${email}`);
    const codeLabel = await waiter(t).el(By.className('register-verify__description'));
    await waiter(t).textMatches(codeLabel, /verification code sent to/);
});

test('verification code input label', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/verify/${uniqueData.email()}`);
    const codeLabel = await waiter(t).el(By.className('register-verify__code'));
    await waiter(t).textMatches(codeLabel, /verification code/);
});

test('no verification code', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/verify/${uniqueData.email()}`);
    await waiter(t).el(By.className('register-verify__button')).click();
    const errors = await waiter(t).els(By.className('register-verify__error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('verification code required');
});

test('wrong verification code', async t => {
    const email = uniqueData.email();
    const wrongVerificationCode = uniqueData.verificationCode();
    const badRegisterVerifyMatcher = new MSRequestMatcherBuilder()
        .post().path(`/api/registration/${email}/verification`)
        .body(wrongVerificationCode).build();
    await backend.expectWithPreflight(badRegisterVerifyMatcher, new MSResponseBuilder().status(401).build());
    await driver(t).get(`${process.env.FRONTEND_URI}/register/verify/${email}`);
    await waiter(t).el(By.className('register-verify__code-input')).sendKeys(wrongVerificationCode, Key.ENTER);
    const errors = await waiter(t).els(By.className('register-verify__error'));
    expect(await Promise.all(errors.map(async error => await error.getText())))
        .to.include('wrong verification code');
    await backend.verifyAndClear(badRegisterVerifyMatcher);
});
