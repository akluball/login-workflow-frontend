import { LoggedInContext, driver, waiter, user, userIdCookie, sessionCookie, token } from './test-util';
import _test, { TestInterface } from 'ava';
import { getDriver } from './utils';
import { waitingOn } from './WebDriverWaiter';
import * as backend from './backend/backend';
import * as uniqueData from './unique-data';
import { Cookie } from './CookieBuilder';
import { MSRequestMatcherBuilder, MSResponseBuilder } from './backend/mock-server';
import { By } from 'selenium-webdriver';

const test = _test as TestInterface<LoggedInContext>;

const healthcheckMatcher = new MSRequestMatcherBuilder().get().path('/api/healthcheck').build();

test.before(async t => {
    const healthcheckResponse = new MSResponseBuilder().status(200).body('').build();
    await backend.expect(healthcheckMatcher, healthcheckResponse);
});

test.after.always(async t => {
    await backend.clear(healthcheckMatcher);
});

test.beforeEach(async t => {
    t.context.driver = getDriver();
    t.context.waiter = waitingOn(driver(t));
    t.context.user = uniqueData.user();
    t.context.userIdCookie = new Cookie.Builder()
        .name(backend.CookieNames.USER_ID).value(user(t).id)
        .path('/api/token').build();
    const sessionKey = uniqueData.sessionKey();
    t.context.sessionCookie = new Cookie.Builder()
        .name(backend.CookieNames.SESSION).value(sessionKey)
        .path('/api/token').build();
    t.context.token = uniqueData.token();
    await driver(t).get(`${process.env.MOCK_SERVER_URI}/api/healthcheck`);
    await driver(t).manage().addCookie(userIdCookie(t).asWebDriverCookie());
    await driver(t).manage().addCookie(sessionCookie(t).asWebDriverCookie());
    await driver(t).get(process.env.FRONTEND_URI as string);
    const getTokenMatcher = new MSRequestMatcherBuilder().get().path('/api/token')
        .cookie(backend.CookieNames.USER_ID, '' + user(t).id)
        .cookie(backend.CookieNames.SESSION, sessionKey).build();
    const getUserMatcher = new MSRequestMatcherBuilder().get().path('/api/user')
        .header('Authorization', `Bearer ${token(t)}`).build();
    await backend.expect(getTokenMatcher, new MSResponseBuilder().status(200).body(token(t)).build());
    await backend.expect(getUserMatcher, new MSResponseBuilder().status(200).json(user(t)).build());
});

test.afterEach.always(async t => {
    await driver(t).quit();
    const getTokenMatcher = new MSRequestMatcherBuilder().get().path('/api/token')
        .cookie(backend.CookieNames.USER_ID, '' + user(t).id).build();
    const getUserMatcher = new MSRequestMatcherBuilder().get().path('/api/user')
        .header('Authorization', `Bearer ${token(t)}`).build();
    await backend.clear(getTokenMatcher);
    await backend.clear(getUserMatcher);
});

test('empty path route to home when logged in', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    await waiter(t).uriMatches(/\/home/);
});

test('title', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    await waiter(t).title('Login Workflow - Home');
});

test('header', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    const header = await waiter(t).el(By.className('home__header'));
    await waiter(t).text(header, 'Login Workflow Home');
});

test('handle', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    const header = await waiter(t).el(By.className('home__handle'));
    await waiter(t).text(header, user(t).handle);
});

test('name', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    const header = await waiter(t).el(By.className('home__name'));
    await waiter(t).text(header, `name: ${user(t).firstName} ${user(t).lastName}`);
});

test('email', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    const header = await waiter(t).el(By.className('home__email'));
    await waiter(t).text(header, `email: ${user(t).email}`);
});

test('logout button text', async t => {
    await driver(t).get(process.env.FRONTEND_URI as string);
    const header = await waiter(t).el(By.className('home__logout'));
    await waiter(t).text(header, 'Log Out');
});