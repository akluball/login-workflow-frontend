import { expect } from 'chai';
import * as webdriver from 'selenium-webdriver';
import * as firefox from 'selenium-webdriver/firefox';
import * as chrome from 'selenium-webdriver/chrome';
import { AxiosResponse } from 'axios';

export function getDriver(): webdriver.WebDriver {
    const firefoxServiceBuilder = new firefox.ServiceBuilder();
    const chromeServiceBuilder = new chrome.ServiceBuilder();
    if (process.env.WEBDRIVER_LOGGING as string === '1') {
        webdriver.logging.addConsoleHandler();
        webdriver.logging.getLogger('webdriver.http')
            .setLevel(webdriver.logging.Level.ALL);
        firefoxServiceBuilder.enableVerboseLogging().setStdio('inherit');
        chromeServiceBuilder.enableVerboseLogging().setStdio('inherit');
    }
    const browserLoggingPreferences = new webdriver.logging.Preferences();
    browserLoggingPreferences.setLevel(webdriver.logging.Type.BROWSER, 'finest');
    return new webdriver.Builder()
        .forBrowser(webdriver.Browser.CHROME) // default to chrome, override with SELENIUM_BROWSER=firefox
        .setFirefoxOptions(new firefox.Options().headless())
        .setFirefoxService(firefoxServiceBuilder)
        .setChromeOptions(new chrome.Options().headless())
        .setChromeService(chromeServiceBuilder)
        .setLoggingPrefs(browserLoggingPreferences)
        .build();
}

export async function printBrowserConsole(driver: webdriver.WebDriver) {
    const logs = await driver.manage()
        .logs()
        .get(webdriver.logging.Type.BROWSER);
    logs.forEach(log => console.log(log.message));
}

export function sleep(seconds: number): Promise<void> {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, seconds * 1000);
    });
}

function toHexString(asDecimalString: string): string {
    const asDecimal = Number.parseInt(asDecimalString);
    let asString = asDecimal.toString(16).toUpperCase();
    if (asString.length === 1)
        asString = '0' + asString;
    return asString;
}

const rgbaRegex = /rgba?\(([12]?[0-9]?[0-9]), ?([12]?[0-9]?[0-9]), ?([12]?[0-9]?[0-9]),? ?.*\)/;

export async function expectColor(el: webdriver.WebElement, expectedColor: string) {
    const rgbColor = await el.getCssValue('color');
    const rgbaRegexMatch = rgbColor.match(rgbaRegex);
    if (rgbaRegexMatch === null) {
        throw new Error('Unable to parse rgb: ' + rgbColor);
    } else {
        const actualHexColor = '#'
            + toHexString(rgbaRegexMatch[1])
            + toHexString(rgbaRegexMatch[2])
            + toHexString(rgbaRegexMatch[3]);
        expect(actualHexColor).to.equal(expectedColor);
    }
}

export function minutesToMillis(minutes: number) {
    return minutes * 60 * 1000;
}

export function millisToSeconds(millis: number) {
    return millis / 1000;
}

export function is2xx(response: AxiosResponse) {
    return response.status.toString().startsWith('2');
}