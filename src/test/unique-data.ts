import { Registration, User } from './types';
import { millisToSeconds } from './utils';

const pid = process.pid;

let firstNameCounter = 0;
export function firstName() {
    return `firstname${pid}pid${firstNameCounter++}`;
}

let lastNameCounter = 0;
export function lastName() {
    return `lastname${pid}pid${lastNameCounter++}`;
}

let handleCounter = 0;
export function handle() {
    return `handle${pid}pid${handleCounter++}`;
}

let passwordCounter = 0;
export function password() {
    return `pass${pid}pid${passwordCounter++}`;
}

let emailCounter = 0;
export function email() {
    return `user${pid}pid${emailCounter++}@example.com`;
}

export function registration(): Registration {
    return {
        email: email(),
        handle: handle(),
        password: password(),
        firstName: firstName(),
        lastName: lastName()
    };
}

let verificationCodeCounter = 0;
export function verificationCode() {
    return `code${pid}pid${verificationCodeCounter++}`;
}

let userIdCounter = 1;
export function userId() {
    return (pid * 1000) + userIdCounter++;
}

export function user(): User {
    return {
        id: userId(),
        email: email(),
        handle: handle(),
        firstName: firstName(),
        lastName: lastName(),
        joinEpochSeconds: millisToSeconds(Date.now())
    };
}

let sessionKeyCounter = 0;
export function sessionKey() {
    return `session${pid}pid${sessionKeyCounter++}`;
}

let tokenCounter = 0;
export function token() {
    const exp = (Date.now() + 6000) / 1000;
    const payload = Buffer.from((JSON.stringify({ exp }))).toString('base64');
    return `header${pid}.${payload}.signature${tokenCounter++}`;
}