import { IWebDriverOptionsCookie } from 'selenium-webdriver';
import * as backend from './backend/backend';
import { minutesToMillis } from './utils';

const domain = backend.domain;

export class Cookie {
    private name: string | undefined;
    private value: string | undefined;
    private path: string | undefined;
    private httpOnly: boolean = true;
    private maxAgeSeconds: number = minutesToMillis(60);

    asWebDriverCookie(): IWebDriverOptionsCookie {
        const name = this.name || '';
        const value = this.value || '';
        const path = this.path || '';
        return {
            name,
            value,
            domain,
            path,
            httpOnly: this.httpOnly,
            expiry: Date.now() + this.maxAgeSeconds
        };
    }

    asSetCookieHeader(): string {
        const name = this.name || '';
        const value = this.value || '';
        const path = this.path || '';
        return `${name}=${value};Domain=${domain};Path=${path};Max-Age=${this.maxAgeSeconds}${(this.httpOnly ? ';HttpOnly' : '')}`;
    }

    static Builder = class Builder {
        private cookie: Cookie;

        constructor() {
            this.cookie = new Cookie();
        }

        name(name: string): Builder {
            this.cookie.name = name;
            return this;
        }

        value(value: string | number): Builder {
            if (typeof value === 'number') {
                this.cookie.value = '' + value;
            } else {
                this.cookie.value = value;
            }
            return this;
        }

        path(path: string): Builder {
            this.cookie.path = path;
            return this;
        }

        build(): Cookie {
            return this.cookie;
        }
    }
}