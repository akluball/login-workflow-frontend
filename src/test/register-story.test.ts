import test from 'ava';
import { By, Key, WebDriver, WebElement } from 'selenium-webdriver';
import * as backend from './backend/backend';
import * as uniqueData from './unique-data';
import { getDriver } from './utils';
import { waitingOn, WebDriverWaiter } from './WebDriverWaiter';
import { MSRequestMatcher, MSRequestMatcherBuilder } from './backend/mock-server';
import { Registration } from './types';

let driver: WebDriver;
let waiter: WebDriverWaiter;

test.before(t => {
    driver = getDriver();
    waiter = waitingOn(driver);
});

test.after.always(async t => {
    await driver.quit();
});

test.serial('login page link', async t => {
    await driver.get(`${process.env.FRONTEND_URI}/login`);
    const registerLink = await waiter.el(By.className('login__register-link'));
    await waiter.textMatches(registerLink, /^register/);
    await registerLink.click();
    await waiter.uriMatches(/\/register\/input\/name$/);
    await waiter.title('Login Workflow - Register Input');
});

const registration = uniqueData.registration();

test.serial('first name input', async t => {
    await waiter.el(By.className('register-name__first-input'))
        .sendKeys(registration.firstName);
});

test.serial('last name input', async t => {
    await waiter.el(By.className('register-name__last-input'))
        .sendKeys(registration.lastName, Key.ENTER);
});

test.serial('handle input', async t => {
    await waiter.el(By.className('register-creds__handle-input'))
        .sendKeys(registration.handle);
});

test.serial('password input', async t => {
    await waiter.el(By.className('register-creds__password-input'))
        .sendKeys(registration.password);
});

test.serial('retype password input', async t => {
    await waiter.el(By.className('register-creds__retype-password-input'))
        .sendKeys(registration.password, Key.ENTER);
});

test.serial('email input', async t => {
    await waiter.el(By.className('register-email__email-input'))
        .sendKeys(registration.email);
});

const registerMatcher = new MSRequestMatcherBuilder()
    .post().path('/api/registration')
    .json(registration).build();

test.serial('send verification code', async t => {
    const sendButton = await waiter.el(By.className('register-email__send'));
    await backend.expect(registerMatcher);
    await sendButton.click();
    await waiter.uriMatches(/\/register\/verify/);
    await backend.verifyAndClear(registerMatcher);
    await backend.clear(registerMatcher);
});

const verificationCode = uniqueData.verificationCode();

const registerVerifyMatcher = new MSRequestMatcherBuilder()
    .post().path(`/api/registration/${registration.email}/verification`)
    .body(verificationCode).build();

test.serial('verify', async t => {
    const button = await waiter.el(By.className('register-verify__button'));
    await waiter.text(button, 'verify');
    await backend.expect(registerVerifyMatcher);
    const input = waiter.el(By.className('register-verify__code-input'));
    await input.clear();
    await input.sendKeys(verificationCode);
    await button.click();
    await waiter.uriMatches(/\/register\/success/);
    await backend.verifyAndClear(registerVerifyMatcher);
    await backend.clear(registerVerifyMatcher);
});

test.serial('success', async t => {
    const successHeader = await waiter.el(By.className('register-success__header'));
    await waiter.text(successHeader, 'Registration Success');
    const loginLink = await waiter.el(By.className('register-success__login-link'));
    await waiter.textMatches(loginLink, /Log In/);
});