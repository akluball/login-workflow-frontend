import { WebDriverWaiter } from './WebDriverWaiter';
import avaTest, { TestInterface, ExecutionContext } from 'ava';
import { WebDriver } from 'selenium-webdriver';
import { User } from './types';
import { Cookie } from './CookieBuilder';

export interface DriverAndWaiterContext {
    driver: WebDriver;
    waiter: WebDriverWaiter;
}

export interface LoggedInContext extends DriverAndWaiterContext {
    user: User;
    userIdCookie: Cookie;
    sessionCookie: Cookie;
    token: string;
}

export const test = avaTest as TestInterface<DriverAndWaiterContext>;

export function driver<T extends DriverAndWaiterContext>(t: ExecutionContext<T>): WebDriver {
    return t.context.driver;
}

export function waiter<T extends DriverAndWaiterContext>(t: ExecutionContext<T>): WebDriverWaiter {
    return t.context.waiter;
}

export function user<T extends LoggedInContext>(t: ExecutionContext<T>): User {
    return t.context.user;
}

export function userIdCookie<T extends LoggedInContext>(t: ExecutionContext<T>): Cookie {
    return t.context.userIdCookie;
}

export function sessionCookie<T extends LoggedInContext>(t: ExecutionContext<T>): Cookie {
    return t.context.sessionCookie;
}

export function token<T extends LoggedInContext>(t: ExecutionContext<T>): string {
    return t.context.token;
}