import * as backend from './backend/backend';
import { MSRequestMatcherBuilder, MSResponseBuilder } from './backend/mock-server';

const noUserIdCookieTokenMatcher = new MSRequestMatcherBuilder()
    .get().path('/api/token')
    .cookie(`!${backend.CookieNames.USER_ID}`, '.*').build();

const noSessionCookieMatcher = new MSRequestMatcherBuilder()
    .get().path('/api/token')
    .cookie(`!${backend.CookieNames.SESSION}`, '.*').build();

(async () => {
    await backend.reset();
    await backend.expect(noUserIdCookieTokenMatcher, new MSResponseBuilder().status(401).build());
    await backend.expect(noSessionCookieMatcher, new MSResponseBuilder().status(401).build());
})().catch(console.error);