import { driver, waiter, DriverAndWaiterContext } from './test-util';
import _test, { TestInterface } from 'ava';
import { By } from 'selenium-webdriver';
import { getDriver } from './utils';
import { waitingOn } from './WebDriverWaiter';

const test = _test as TestInterface<DriverAndWaiterContext>;

test.beforeEach(t => {
    t.context.driver = getDriver();
    t.context.waiter = waitingOn(driver(t));
});

test.afterEach.always(async t => {
    await driver(t).quit();
});

test('title', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/not/a/real/page`);
    await waiter(t).title('Login Workflow - Page not Found');
});

test('message', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/not/a/real/page`);
    const notFoundMessage = await waiter(t).el(By.className('not-found__msg'));
    await waiter(t).text(notFoundMessage, 'Page not found: /register/not/a/real/page');
});

test('Log in link', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/register/not/a/real/page`);
    await waiter(t).el(By.className('not-found__link')).click();
    await waiter(t).title('Login Workflow - Log In');
});