import test from 'ava';
import { WebDriver, By, WebElement } from 'selenium-webdriver';
import * as backend from './backend/backend';
import * as uniqueData from './unique-data';
import { getDriver, printBrowserConsole } from './utils';
import { waitingOn, WebDriverWaiter } from './WebDriverWaiter';
import { MSResponse, MSRequestMatcher, MSResponseBuilder, MSRequestMatcherBuilder } from './backend/mock-server';
import { Cookie } from './CookieBuilder';
import { User } from './types';

let driver: WebDriver;
let waiter: WebDriverWaiter;

test.before(t => {
    driver = getDriver();
    waiter = waitingOn(driver);
});

test.after.always(async t => {
    await driver.quit();
});

const user = uniqueData.user();
const password = uniqueData.password();
const sessionKey = uniqueData.sessionKey();

test.serial('identifier', async t => {
    driver.get(`${process.env.FRONTEND_URI}/login`);
    await waiter.uriMatches(/\/login/);
    const identifierLabel = await waiter.el(By.className('login__identifier'));
    await waiter.text(identifierLabel, 'email or handle:');
    const identifierInput = await waiter.el(By.className('login__identifier-input'));
    await identifierInput.clear();
    await identifierInput.sendKeys(user.email);
});

test.serial('password', async t => {
    const passwordLabel = await waiter.el(By.className('login__password'));
    await waiter.text(passwordLabel, 'password:');
    const passwordInput = await waiter.el(By.className('login__password-input'));
    await passwordInput.clear();
    await passwordInput.sendKeys(password);
});

const userIdCookie = new Cookie.Builder()
    .name(backend.CookieNames.USER_ID).value(user.id)
    .path('/api/token').build();
const userIdLogoutCookie = new Cookie.Builder()
    .name(backend.CookieNames.USER_ID_LOGOUT).value(user.id)
    .path('/api/logout').build();
const sessionCookie = new Cookie.Builder()
    .name(backend.CookieNames.SESSION).value(sessionKey)
    .path('/api/token').build();
const sessionLogoutCookie = new Cookie.Builder()
    .name(backend.CookieNames.SESSION_LOGOUT).value(sessionKey)
    .path('/api/logout').build();
const loginMatcher = new MSRequestMatcherBuilder().post().path(`/api/login/${user.email}`)
    .body(password).header('Content-Type', 'application/json').build();
const loginResponse = new MSResponseBuilder().status(200)
    .header('Set-Cookie', userIdCookie.asSetCookieHeader())
    .header('Set-Cookie', userIdLogoutCookie.asSetCookieHeader())
    .header('Set-Cookie', sessionCookie.asSetCookieHeader())
    .header('Set-Cookie', sessionLogoutCookie.asSetCookieHeader())
    .build();

const token = uniqueData.token();
const getTokenMatcher = new MSRequestMatcherBuilder().get().path('/api/token')
    .cookie(backend.CookieNames.USER_ID, '' + user.id)
    .cookie(backend.CookieNames.SESSION, sessionKey).build();
const tokenResponse = new MSResponseBuilder().status(200).body(token).build();

const getCurrentUserMatcher = new MSRequestMatcherBuilder().get().path('/api/user')
    .header('Authorization', `Bearer ${token}`).build();
const currentUserResponse = new MSResponseBuilder().status(200)
    .body(JSON.stringify(user)).build();

test.serial('login', async t => {
    await backend.expectWithPreflight(loginMatcher, loginResponse);
    await backend.expect(getTokenMatcher, tokenResponse);
    await backend.expect(getCurrentUserMatcher, currentUserResponse);
    const loginButton = await waiter.el(By.className('login__button'));
    await waiter.text(loginButton, 'Log In');
    await loginButton.click();
    await waiter.uriMatches(/\/home/);
    await waiter.title('Login Workflow - Home');
    await backend.verifyAndClear(loginMatcher);
    await backend.verifyAndClear(getTokenMatcher);
    await backend.verifyAndClear(getCurrentUserMatcher);
});

test.serial('logout', async t => {
    const logoutMatcher = new MSRequestMatcherBuilder().post().path('/api/logout')
        .cookie(backend.CookieNames.USER_ID_LOGOUT, '' + user.id)
        .cookie(backend.CookieNames.SESSION_LOGOUT, sessionKey)
        .build();
    await backend.expectWithPreflight(logoutMatcher, new MSResponseBuilder().status(204).build());
    await backend.expect(getTokenMatcher, new MSResponseBuilder().status(401).build());
    await waiter.el(By.className('home__logout')).click();
    await waiter.uriMatches(/\/login/);
    await waiter.title('Login Workflow - Log In');
    await backend.verifyAndClear(logoutMatcher);
    await backend.clear(getTokenMatcher);
});