export interface Registration {
    handle: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
}

export interface User {
    id: number;
    handle: string;
    email: string;
    firstName: string;
    lastName: string;
    joinEpochSeconds: number;
}