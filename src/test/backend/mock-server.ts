import HttpMethod from './HttpMethod';

export interface MSHeaders {
    [index: string]: string[];
}

export interface MSCookie {
    name: string;
    value: string;
}

export interface MSRequestJsonBody {
    type: string;
    json: string;
}

export interface MSRequestMatcher {
    path: string;
    method: string;
    body?: MSRequestJsonBody | string;
    headers?: MSHeaders;
    cookies?: MSCookie[];
}

export interface MSResponse {
    statusCode: number;
    body?: string;
    headers?: MSHeaders;
}

export class MSRequestMatcherBuilder {
    private _path: string = '';
    private _method: HttpMethod = HttpMethod.GET;
    private _body: string | MSRequestJsonBody | undefined;
    private _headers: MSHeaders = {};
    private _cookies: MSCookie[] = [];

    get(): MSRequestMatcherBuilder {
        this._method = HttpMethod.GET;
        return this;
    }

    post(): MSRequestMatcherBuilder {
        this._method = HttpMethod.POST;
        return this;
    }

    path(path: string): MSRequestMatcherBuilder {
        this._path = path;
        return this;
    }

    json(unserialized: object): MSRequestMatcherBuilder {
        this._body = { type: 'JSON', json: JSON.stringify(unserialized) };
        return this;
    }

    body(body: string): MSRequestMatcherBuilder {
        this._body = body;
        return this;
    }

    header(name: string, value: string) {
        this._headers[name] = this._headers[name] || [];
        this._headers[name].push(value);
        return this;
    }

    cookie(name: string, value: string): MSRequestMatcherBuilder {
        this._cookies.push({ name, value });
        return this;
    }

    build(): MSRequestMatcher {
        return {
            path: this._path,
            method: this._method,
            body: this._body,
            headers: this._headers,
            cookies: this._cookies
        };
    }
}

const frontendUri = process.env.FRONTEND_URI as string;

export class MSResponseBuilder {
    private _statusCode: number = 200;
    private _body: string | undefined;
    private _headers: MSHeaders;

    constructor() {
        this._headers = {
            'Access-Control-Allow-Origin': [frontendUri as string],
            'Access-Control-Allow-Method': ['GET', 'PUT', 'POST', 'DELETE', 'UPDATE', 'OPTIONS'],
            'Access-Control-Allow-Headers': ['X-Requested-With', 'X-HTTP-Method-Override', 'Content-Type', 'Accept', 'Authorization'],
            'Access-Control-Allow-Credentials': ['true'],
            // force browser to not cache preflight requests
            // this forces developer to always call expectWithPreflight where it is required rather than relying on a previous test having called it
            'Access-Control-Max-Age': ['-1']
        };
    }

    status(status: number): MSResponseBuilder {
        this._statusCode = status;
        return this;
    }

    json(unserialized: object): MSResponseBuilder {
        this._body = JSON.stringify(unserialized);
        return this;
    }

    body(body: string): MSResponseBuilder {
        this._body = body;
        return this;
    }

    header(name: string, value: string): MSResponseBuilder {
        this._headers[name] = this._headers[name] || [];
        this._headers[name].push(value);
        return this;
    }

    build(): MSResponse {
        return {
            statusCode: this._statusCode,
            body: this._body,
            headers: this._headers,
        };
    }
}