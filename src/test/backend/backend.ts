import axios from 'axios';
import { sleep, is2xx } from '../utils';
import { MSRequestMatcher, MSResponse, MSRequestMatcherBuilder, MSResponseBuilder } from './mock-server';
import HttpMethod from './HttpMethod';

const mockServerUri = `${process.env.MOCK_SERVER_URI}/mockserver`;

export const domain = process.env.BACKEND_DOMAIN as string;

export enum CookieNames {
    USER_ID = 'login-workflow-user-id',
    USER_ID_LOGOUT = 'login-workflow-user-id-logout',
    SESSION = 'login-workflow-session',
    SESSION_LOGOUT = 'login-workflow-session-logout'
}

class ExpectedRequestFailure extends Error {
    constructor(message: string) {
        super(message);
    }
}

function okOr406Validator(status: number): boolean {
    return (status.toString().startsWith('2')) || (status === 406);
}

export async function reset() {
    await axios.put(`${mockServerUri}/reset`);
}

export async function expectWithPreflight(requestMatcher: MSRequestMatcher, response: MSResponse = { statusCode: 200 }) {
    await expect({ path: requestMatcher.path, method: HttpMethod.OPTIONS });
    await expect(requestMatcher, response);
}

export async function expect(requestMatcher: MSRequestMatcher, response: MSResponse = new MSResponseBuilder().build()) {
    await axios.put(`${mockServerUri}/expectation`, { httpRequest: requestMatcher, httpResponse: response });
}

const verifyRetryLimit = 5;
const verifyRetryWaitSeconds = 1;

export async function verifyAndClear(requestMatcher: MSRequestMatcher) {
    await verify(requestMatcher);
    await clear(requestMatcher);
}

export async function verify(requestMatcher: MSRequestMatcher) {
    const matcher = { httpRequest: requestMatcher, times: { atLeast: 1 } };
    for (let tries = 0; tries < verifyRetryLimit; tries++) {
        const response = await axios.put(`${mockServerUri}/verify`, matcher, { validateStatus: okOr406Validator });
        if (is2xx(response)) {
            return
        } else if (tries === verifyRetryLimit - 1) {
            throw new ExpectedRequestFailure(response.data);
        }
        await sleep(verifyRetryWaitSeconds);
    }
}

export async function clear(requestMatcher: MSRequestMatcher) {
    await axios.put(`${mockServerUri}/clear`, requestMatcher);
    // clear all pre-flight requests to the given path
    // reasonably assumes we do not care about pre-flight request records
    await axios.put(`${mockServerUri}/clear`, { path: requestMatcher.path, method: HttpMethod.OPTIONS });
}

export async function clearRequests(requestMatcher: MSRequestMatcher) {
    await axios.put(`${mockServerUri}/clear?type=log`, requestMatcher);
    // clear all pre-flight requests to the given path
    // reasonably assumes we do not care about pre-flight request records
    await axios.put(`${mockServerUri}/clear?type=log`, { path: requestMatcher.path, method: HttpMethod.OPTIONS });
}

export const NO_COOKIES_TOKEN_MATCHER = new MSRequestMatcherBuilder().get().path('/api/token')
    .cookie(`!${CookieNames.USER_ID}`, '.*')
    .cookie(`!${CookieNames.SESSION}`, '.*').build();