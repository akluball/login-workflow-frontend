enum HttpMethod {
    GET = 'GET',
    OPTIONS = 'OPTIONS',
    POST = 'POST'
}

export default HttpMethod