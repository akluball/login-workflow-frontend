import { driver, waiter, DriverAndWaiterContext } from './test-util';
import _test, { TestInterface } from 'ava';
import { By, WebElement, Key } from 'selenium-webdriver';
import { waitingOn } from './WebDriverWaiter';
import { getDriver } from './utils';
import { MSResponseBuilder, MSRequestMatcher, MSRequestMatcherBuilder } from './backend/mock-server';
import * as backend from './backend/backend';
import * as uniqueData from './unique-data';

const test = _test as TestInterface<DriverAndWaiterContext>;

test.beforeEach(t => {
    t.context.driver = getDriver();
    t.context.waiter = waitingOn(driver(t));
});

test.afterEach.always(async t => {
    await driver(t).quit();
});

test.serial('no token, /home routes to /login', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI as string}/home`);
    await waiter(t).uriMatches(/\/login/);
    await waitingOn(driver(t)).title('Login Workflow - Log In');
    await backend.clearRequests(backend.NO_COOKIES_TOKEN_MATCHER);
});

test('email or handle required', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/login`);
    await waiter(t).el(By.className('login__password-input')).sendKeys(uniqueData.password(), Key.ENTER);
    const loginErr = await waiter(t).el(By.className('login__below-identifier-err'));
    await waiter(t).text(loginErr, 'email or handle required');
});

test('password required', async t => {
    await driver(t).get(`${process.env.FRONTEND_URI}/login`);
    await waiter(t).el(By.className('login__identifier-input')).sendKeys(uniqueData.email(), Key.ENTER);
    const loginErr = await waiter(t).el(By.className('login__below-password-err'));
    await waiter(t).text(loginErr, 'password required');
});

test('bad credentials', async t => {
    const email = uniqueData.email();
    const password = uniqueData.password();
    const badLoginMatcher = new MSRequestMatcherBuilder()
        .post().path(`/api/login/${email}`).body(password)
        .header('Content-Type', 'application/json').build();
    await backend.expectWithPreflight(badLoginMatcher, new MSResponseBuilder().status(401).build());
    await driver(t).get(`${process.env.FRONTEND_URI}/login`);
    await waiter(t).el(By.className('login__identifier-input')).sendKeys(email);
    await waiter(t).el(By.className('login__password-input')).sendKeys(password, Key.ENTER);
    await backend.verifyAndClear(badLoginMatcher);
    const loginErr = await waiter(t).el(By.className('login__below-button-err'));
    await waiter(t).text(loginErr, 'invalid credentials');
    await backend.clearRequests(backend.NO_COOKIES_TOKEN_MATCHER);
});