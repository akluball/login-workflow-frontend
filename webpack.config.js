const path = require('path');
const { EnvironmentPlugin } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const bundleName = 'app.bundle.js';

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    context: path.resolve(__dirname, 'src', 'main'),
    entry: './app.ts',
    output: {
        path: path.resolve(__dirname, 'build', 'main'),
        filename: bundleName
    },
    module: {
        rules: [
            {
                test: /\.tsx?/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                    configFile: path.resolve(__dirname, 'src', 'main', 'tsconfig.json')
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]

            }, {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }

        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
        alias: { vue: 'vue/dist/vue.esm.js' }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'main', 'index.html'),
            templateParameters: { bundle: bundleName },
            inject: false
        }),
        new VueLoaderPlugin(),
        new EnvironmentPlugin(['BACKEND_URI'])
    ]
};