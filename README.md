# Login Workflow Frontend

The frontend for the [login-workflow] application.
The login-workflow application is an example of a containerized workflow.

This project requires a linux host with docker and npm/node.

## Test

To run tests:
```
npm run test-runner
```
The steps performed by the test runner script are:
1. Create docker network all containers will run on
2. Start mock backend container (MockServer)
3. Build frontend image and run
4. Start selenium standalone container
5. Build test image and run
6. Delete containers and network
7. Exit the test runner script with the exit code of the test container

This does not lead to a rapid workflow motivating development mode.

## Development Mode Test

To run tests in development mode
```
npm run test-runner:dev
```
The steps performed by the development mode test runner script are similar to the steps described above.
However, frontend and test compilers (running in watch mode) are run in separate containers.
The relevant source components are mounted into the compiler containers so changes are picked up by them.
Named volumes are mounted, encapsulating the compile destinations of the compiler containers.
These named volumes are then mounted into the appropriate locations of the frontend and test container.
Therefore, the frontend and test containers see changes to source code as quickly the watch mode compilers see them.
Finally, the entrypoint of the test container is overridden with an interactive shell session.
Then tests can be run in container as much as desired using:
```
npm test
```
When finished with your development session, simply exit the test container shell.

The following pertains to viewing compiler logs when running tests in development mode.
These commands must be run on the host (not in container like the npm test command above).
To view logs of the frontend compiler container:
```
npm run compiler-logs:dev
```
Likewise for the logs of the test compiler container:
```
npm run test-compiler-logs:dev
```
These npm scripts simply wrap `docker logs` calls.
Therefore, you can pass any `docker logs` options to the npm run calls.
So if you want to follow the compile logs:
```
npm run compiler-logs:dev -- --follow
```

## Test Runner Logging

Most logging is suppressed in the test runner such as the output from docker build.
To turn this logging on, use environment `LOGIN_WORKFLOW_LOGGING=1`.
So to run tests with docker build logging:
```
LOGIN_WORKFLOW_LOGGING=1 npm run test-runner
```

## Docker Image Cleanup

Remove built images
```
npm run rm-built-images
```

Remove dependency images
```
npm run rm-dependency-images
```

## Notes

Mock Server requests occasionally terminate with socket hang up.

[login-workflow]: https://gitlab.com/akluball/login-workflow
